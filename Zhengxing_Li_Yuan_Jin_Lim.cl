(defun sigmoid (x)
  (/ 1 (1+ (exp (- x))))  
)

(defun sigmoid_m (a)

    (let*
        ( 
            (m          (nth 0 (array-dimensions a))    )
            (n          (nth 1 (array-dimensions a))    )
            (results    (make-array (list m n))         )
        )
        (dotimes (i m)
            (dotimes (j n)
                (setf (aref results i j) (sigmoid (aref a i j )))
            )
        )   
        results
    )
)

(defun multiply (a b)
    (let*
        ( 
            (m          (nth 0 (array-dimensions a))    )
            (s          (nth 1 (array-dimensions a))    )
            (n          (nth 1 (array-dimensions b))    )
            (results    (make-array (list m n))         )
        )
        (dotimes (i m)
            (dotimes (j n)
                (setf (aref results i j) 0.0)
                (dotimes (k s)
                    (incf (aref results i j) (* (aref a i k) (aref b k j)))
                )
            )
        )
        results
    )
)

(defun scalar_multiply (s x)
    (let*
        ( 
            (m          (nth 0 (array-dimensions x))    )
            (results    (make-array m)         )
        )
        (dotimes (i m)
            (setf (aref results i) (* (aref x i) s))
        )
        results
    )
)

(defun plus_1d (a b)
    (let*
        ( 
            (m          (nth 0 (array-dimensions a))    )
            (results    (make-array m)         )
        )
        (dotimes (i m)
            (setf (aref results i) (+ (aref a i) (aref b i)))
        )
        results
    )
)

(defun minus (a b)
    (let*
        ( 
            (m          (nth 0 (array-dimensions a))    )
            (n          (nth 1 (array-dimensions a))    )
            (results    (make-array (list m n))         )
        )
        (dotimes (i m)
            (dotimes (j n)
                (setf (aref results i j) (- (aref a i j) (aref b i j)))
            )
        )
        results
    )
)

(defun plus (a b)
    (let*
        ( 
            (m          (nth 0 (array-dimensions a))    )
            (n          (nth 1 (array-dimensions a))    )
            (results    (make-array (list m n))         )
        )
        (dotimes (i m)
            (dotimes (j n)
                (setf (aref results i j) (+ (aref a i j) (aref b i j)))
            )
        )
        results
    )
)

(defun prepend1 (x)
    (let*
        (
            (m          (nth 0          (array-dimensions x)          ))
            (n          (+ 1 (nth 1     (array-dimensions x)         )))
            (results    (make-array     (list m n)                    ))
        )
        (dotimes (i m)
            (dotimes (j n)
                (cond 
                    ((= j 0)    (setf (aref results i j) 1))
                    (t          (setf (aref results i j) (aref x i (- j 1))))
                ) 
            )
        )
    results
    )
)

;;; Reference: https://batsov.com/articles/2011/04/30/parsing-numbers-from-string-in-lisp/
(defun parse_float(str)
    (with-input-from-string (in str)
        (loop for x = (read in nil nil) while x collect x)
    )
)

;;; Reference: https://stackoverflow.com/questions/33138087/converting-list-of-strings-to-list-of-ints-in-common-lisp
(defun split-str (string &optional (r nil))
  (let ((n (position "," string
                     :from-end t
                     :test #'(lambda (x y)
                               (find y x :test #'string=)))))
    (if n
        (split-str (subseq string 0 n)
                   (cons (subseq string (1+ n)) r))
        (cons string r))))


;;; Reference: http://clhs.lisp.se/Body/f_rd_lin.htm
(defun read_x (f)
    (setf row 1000)
    (setf col 5)
    (setf list (make-array (list row col)))
    (setf f (open f))
    (dotimes (i row)
        (setf text (read-line f))
        (setf str (split-str text))
        (dotimes (j col)
            (setf current (car str))
            (setf str (cdr str))
            (setf (aref list i j) (car (parse_float current)))
        ) 
    )
    (close f)
    (setf (aref list 0 0) 0)
    list
)

(defun read_d(f)
    (setf row 1000)
    (setf col 1)
    (setf list (make-array row))
    (setf f (open f))
    (dotimes (i row)
        (setf text (read-line f))
        (if (> (length text) 1)
            (setf text (subseq text 1 2))
        )
        (setf (aref list i)  (parse-integer text))
    )
    (close f)
    (setf results (make-array (list row col)))
    (dotimes (m row)
        (dotimes (n col)
            (setf (aref results m n) (aref list m))
        )
    )
    results
)

(defun get_first_gen (pop_size c_size)
    (let*
        (
            (population (make-array (list pop_size c_size)))
        )
        (dotimes (i pop_size)
            (dotimes (j c_size)
                (setf (aref population i j) (float (round-to (- (random 1.0) 0.5) 3)) )
            )
        )
        population
    )
)

(defun get_row (data row)
    (let*
        ( 
            (m          (nth 0 (array-dimensions data))    )
            (n          (nth 1 (array-dimensions data))    )
            (results    (make-array n)         )
        )
        (dotimes (i m)
            (if (= i (- row 1))
                (dotimes (j n)
                    (setf (aref results j)  (aref data i j ))
                )   
            )
        )   
        results
    )
)

(defun my_decoding (c size flag)
    (let*
        (
            (m (nth 0 size))
            (n (nth 1 size))
            (results (make-array (list m n)))
        )
        (dotimes (i m)
            (dotimes (j n)
                (if (equal flag "A")
                    (setf (aref results i j) (aref c (+ i 18)))
                    (setf (aref results i j) (aref c (+ j (* i n))))
                )
            )
        )
        results
    )
)

(defun my_encoding (a b)
    (let*
        (
            (m (nth 0 (array-dimensions b)))
            (n (nth 1 (array-dimensions b)))
            (o (nth 0 (array-dimensions a)))
            (p (nth 1 (array-dimensions a)))
            (results (make-array (+ (* m n) (* o p))))
        )
        (dotimes (i m)
            (dotimes (j n)
                (setf (aref results (+ j (* i n))) (aref b i j))
            )
        )
        (dotimes (k o)
            (dotimes (l p)
                (setf (aref results (+ (* m n) (+ l (* k p)))) (aref a k l))
            )
        )
        results
    )
)

(defun fitness (error)
    (/ 1 error)
)

;;; Reference: http://www.codecodex.com/wiki/Round_a_number_to_a_specific_decimal_place
(defun round-to (number precision &optional (what #'round))
    (let ((div (expt 10 precision)))
         (/ (funcall what (* number div)) div)))

(defun pop_fitness (population x d)
    (let*
        (
            (pop_size (nth 0 (array-dimensions population)))
            (results (make-array pop_size))
        )
        (dotimes (i pop_size)
            (setf row (get_row population (+ i 1)))
            (setf b (my_decoding row '(6 3) "B"))
            (setf a (my_decoding row '(4 1) "A"))
            (setf o (2L_NN x a b))
            (setf err (get_sum_of_squared_error d o))
            (setf f (fitness err))
            (setf (aref results i) f)
        )
        results
    )
)


;;; Reference: https://stackoverflow.com/questions/7508450/whats-the-best-way-to-sort-a-hashtable-by-value
(defun hash-table-alist (table)
  "Returns an association list containing the keys and values of hash table TABLE."
  (let ((alist nil))
    (maphash (lambda (k v)
               (push (cons k v) alist))
             table)
    alist))

(defun hash-table-top-n-values (table n)
  "Returns the top N entries from hash table TABLE. Values are expected to be numeric."
  (subseq (sort (hash-table-alist table) #'> :key #'cdr) 0 n))

(defun sus (population pop_fitness n)
    (let*
        (
            (pop_size (nth 0 (array-dimensions population)))
            (pop_length (nth 1 (array-dimensions population)))

            (total_fitness 0.0)
            (avg_fitness 0.0)
            (all_fitness (make-hash-table :test 'equal))
            (cumulative_fitness (make-array (+ 1 pop_size)))
            (c_fitness_intervals (make-array n))
            (selected_f '())
            (results (make-array n))
        )
        (dotimes (i pop_size)
            (incf total_fitness (aref pop_fitness i))
            (setf (gethash i all_fitness) (aref pop_fitness i))
        )

        (setf avg_fitness (/ total_fitness n))
        (format t"~%        Total F: ~S" total_fitness)
        (format t"~%        N: ~S" n)
        (format t"~%        F/N: ~S" avg_fitness)
        (format t"~%        unsorted fitness: ~S" all_fitness)

        (setf all_fitness (hash-table-top-n-values all_fitness pop_size))
        (setf r (random avg_fitness))
        (format t"~%        r: ~S" r)
        (dotimes (i n)
            (setf (aref c_fitness_intervals i) (+ (* i avg_fitness) r))
        )
        (setf temp (make-hash-table :test 'equal))
        (setf left all_fitness)
        (dotimes (i (length all_fitness))
            (setf current (pop left))
            (setf (gethash (pop current) temp) current)
        )
        (setf all_fitness temp)
        (format t"~%        sorted fitness: ~S" all_fitness)
        (format t"~%        cumulative fitness intervals: ~S" c_fitness_intervals)
        (setf cf 0.0)
        (setf index 0)
        (setf another_index 0)
        (setf (aref cumulative_fitness index) cf)
        (maphash #'(lambda (key val) 
                        (incf index 1)
                        (incf cf (gethash key all_fitness))
                        (setf (aref cumulative_fitness index) cf)

                        (dotimes (i n)
                            (if (and (> (aref c_fitness_intervals i) (- cf (gethash key all_fitness)))
                                    (< (aref c_fitness_intervals i) cf)  
                                )
                                (setf selected_f (cons key selected_f))
                            )
                        )
                   )
             all_fitness)
        (format t"~%        cumulative fitness : ~S" cumulative_fitness)
        (setf temp2 (reverse selected_f))
        (dotimes (i n)
            (setf row_num (car temp2))
            (setf temp2 (cdr temp2))
            (setf (aref results i) row_num)
        )
        (format t"~%        Selected parents : ~S" results)
                
    results    
    )
)

(defun crossover (population selected_parents)
    (let*
        (
            (m (nth 0 (array-dimensions population)))
            (n (nth 1 (array-dimensions population)))
            (results (make-array (list (/ m 2) n)))
        )
        (dotimes (i (/ m 2))
            (setf p1_index (aref selected_parents (* i 2)))
            (setf p2_index (aref selected_parents (+ (* i 2) 1)))
            (setf p1 (get_row population (+ p1_index 1)))
            (setf p2 (get_row population (+ p2_index 1)))
            (setf r (random 1.0))
            (setf child (plus_1d (scalar_multiply r p1) (scalar_multiply (- 1 r) p2)))
            (dotimes (j n)
                (setf (aref results i j) (aref child j))
            )
        )
        results
    )
)

(defun mutate (offspring)
    (let*
        (
            (m (nth 0 (array-dimensions offspring)))
            (n (nth 1 (array-dimensions offspring)))
            (results (make-array (array-dimensions offspring)))
        )
        (dotimes (i m)
            (dotimes (j n)
                (if (= (random 2) 0)
                    (setf (aref results i j) (aref offspring i j))
                    (setf (aref results i j) 
                            ( + (aref offspring i j) (- (random 0.2) 0.1)))
                )
            )
        )
        results
    )
)

(defun form_new_population (population pop_fitness new_offspring)
    (let*
        (
            (m (nth 0 (array-dimensions population)))
            (n (nth 1 (array-dimensions population)))
            (k (nth 0 (array-dimensions new_offspring)))
            (all_fitness (make-hash-table :test 'equal))
            (results (make-array (array-dimensions population)))
        )

        (dotimes (i k)
            (dotimes (j n)
                (setf (aref results i j) (aref new_offspring i j))
            )
        )
        (dotimes (i m)
            (setf (gethash i all_fitness) (aref pop_fitness i))
        )
        (setf all_fitness (hash-table-top-n-values all_fitness 6))
        (dotimes (i 3)
            (setf current (car all_fitness))
            (setf all_fitness (cdr all_fitness))
            (setf current_row (get_row population (+ (car current) 1)))
            (dotimes (j n)
                (setf (aref results (+ i 3) j) (aref current_row j))
            )
        )

        results
    )
)

(defun genetic_algorithm (iteration_count c_size pop_size input d x_test d_test)
    (let*
        (
            (population (make-array (list pop_size c_size)))
            (selected_parents nil)
            (offspring nil)
            (new_offspring nil)
            (new_population nil)
            (results nil)
            (fittest_c (make-array c_size))
            (fittest_c_error 100000000)
        )
        (setf population (get_first_gen pop_size c_size))

        (dotimes (i iteration_count)
            (format t"~%~%~%~%Iteration ~S:" (+ i 1))
            (format t"~%  Population: ~S" population)

            ; get fitness
            (setf pop_fitness (pop_fitness population input d))
            (format t"~%  Fitness: ~S" pop_fitness)

            ; select parents
            (format t"~%  Selection: " )
            (setf selected_parents (sus population pop_fitness 6))

            ; crossover
            (setf offspring (crossover population selected_parents))
            (format t"~%  Crossover: ~S" offspring)

            ; mutate
            (setf new_offspring (mutate offspring))
            (format t"~%  Mutation: ~S" new_offspring)

            ; form new population: 3 childre + 3 top fittest chromosomes
            (setf new_population (form_new_population population pop_fitness new_offspring))
            (format t"~%  New population: ~S" new_population)
            

            (setf current_fittest_c (get_row new_population 4))
            
            (setf b (my_decoding current_fittest_c '(6 3) "B"))
            (setf a (my_decoding current_fittest_c '(4 1) "A"))
            (setf o (2L_NN x_test a b))
            (setf err (get_sum_of_squared_error d_test o))


            (if (< err fittest_c_error)
                (setf fittest_c_error err)
                (format t"~%  The fittest chromosome hasn't change.")
            )

            (if (= err fittest_c_error)
                (setf fittest_c current_fittest_c)
            )

            (format t"~%  The fittest chromosome: ~S" fittest_c)
            (format t"~%  The fittest chromosome error: ~S" fittest_c_error)

            (setf population new_population)

        )
        
        fittest_c
    )
)

(defun 2L_NN (x a b)
    (let*
        (
            (size_x_0 (nth 0 (array-dimensions x)))
            (size_x_1 (nth 1 (array-dimensions x)))
            (x_1 (make-array (list size_x_0 (+ size_x_1 1))))
            (output nil)
        )
        (setf x_1 (prepend1 x))
        (setf xB (multiply x_1 b))
        (setf h  (sigmoid_m xB))
        (setf h_1 (prepend1 h))
        (setf hA (multiply h_1 A))
        (setf output (sigmoid_m hA))
        output
    )
)

(defun get_sum_of_squared_error (d o)
    (let*
        (
            (m (nth 0 (array-dimensions o)))
            (n (nth 1 (array-dimensions o)))
            (results 0.0)
        )
        (setf errors (minus d o))
        (dotimes (i m)
            (setf temp 0.0)
            (dotimes (j n)
                (incf temp (expt (aref errors i j) 2))
            )
            (incf results temp)
        )
        results
    )
)

(defun split_train_test(x p flag)
    (let*
        (
            (m (nth 0 (array-dimensions x)))
            (n (nth 1 (array-dimensions x)))
            (len 0)
            (results nil)
            (index 0)
            (half_len 0)
        )

        (if (equal flag "train")
            (setf len (* m p))
            (setf len (* (- 1 p) m))
        )

        (setf len (round len))
        (setf half_len (/ len 2))
        (setf results (make-array (list len n)))

        (if (equal flag "train")
            (dotimes (i len)
                (dotimes (j n)
                    (cond 
                        ( (< i half_len) (setf (aref results i j) (aref x i j)) )
                        (t (setf (aref results i j) (aref x (+ (- m len) i) j)))
                    )
                )
            )
            (dotimes (i len)
                (dotimes (j n)
                    (setf (aref results i j) (aref x (+ (/ (- m len) 2) i) j))
                )
            )
        )
        results
    )
)

(defun get_accuracy (o d)
    (let*
        ( 
            (m          (nth 0 (array-dimensions o))    )
            (n          (nth 1 (array-dimensions o))    )
            (results    0.0)         
            (count_correct 0)
        )
        (dotimes (i m)
            (dotimes (j n)
                (setf each_o 0)
                (if (>= (aref o i j ) 0.5)
                    (setf each_o 1)
                )

                (if (= (aref d i j) each_o)
                    (incf count_correct 1)
                )
            )
        )
        (setf results (float (/ count_correct m)))   
        results
    )
)

(setf x (read_x "data/input.csv"))
(setf d (read_d "data/desired_o.csv"))

(setf x_train (split_train_test x 0.6 "train"))
(setf d_train (split_train_test d 0.6 "train"))

(setf x_test (split_train_test x 0.6 "test"))
(setf d_test (split_train_test d 0.6 "test"))

(setf optimal_w (genetic_algorithm 40 22 6 x_train d_train x_test d_test))
(setf b (my_decoding optimal_w '(6 3) "B"))
(setf a (my_decoding optimal_w '(4 1) "A"))
(setf o (2L_NN x_train a b))
(setf e (get_sum_of_squared_error d_train o))
(setf fit (fitness e))
(setf accuracy (get_accuracy o d_train))
(format t"~%~%~%~%~%~%~%Optimal Solution:")
(format t"~%    B: ~S." b)
(format t"~%    A: ~S." a)
(format t"~%    Fitness: ~S." fit)
(format t"~%    Accuracy: ~S." accuracy)